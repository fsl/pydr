# Dual Regression for CIFTI

Cluster-aware python implementation of dual-regression for CIFTI images.

see: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/DualRegression
